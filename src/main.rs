#[macro_use] extern crate serde_derive;

use actix_web::{App, HttpServer, HttpResponse};
use actix_web::{get, post, Responder};
use actix_web::web::{Query, Path, Json};

#[get("/version")]
pub async fn version() -> impl Responder {
    "0.1.0"
}

#[get("/version2")]
pub async fn version2() -> impl Responder {
    HttpResponse::Ok().json("0.1.0")
}

#[derive(Deserialize, Serialize)]
pub struct HelloQuery {
    name: String,
}

#[derive(Deserialize, Serialize)]
pub struct HelloResponse {
    resp: String,
}

#[get("/hello/query")]
pub async fn hello_query(params: Query<HelloQuery>) -> impl Responder {
    HttpResponse::Ok().json(format!("Hello, {}", params.name))
}

#[get("/hello/path/{name}")]
pub async fn hello_path(params: Path<String>) -> impl Responder {
    HttpResponse::Ok().json(format!("Hello, {}", params))
}

#[get("/hello/path2/{name}")]
pub async fn hello_path2(params: Path<HelloQuery>) -> impl Responder {
    HttpResponse::Ok().json(format!("Hello, {}", params.name))
}

#[post("/hello/json")]
pub async fn hello_json(params: Json<HelloQuery>) -> impl Responder {
    HttpResponse::Ok().json(HelloResponse { resp: String::from(&params.name) })
}

#[derive(Deserialize, Serialize, Clone)]
pub enum HELLO {
    VAR1,
    VAR2,
    VAR3
}

#[derive(Deserialize, Serialize, Clone)]
pub struct HelloEnum {
    value: HELLO
}

#[post("/hello/enum")]
pub async fn hello_enum(params: Json<HelloEnum>) -> impl Responder {
    HttpResponse::Ok().json(HelloEnum { value: params.clone().value })
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| App::new()
        .service(version)
        .service(version2)
        .service(hello_query)
        .service(hello_path)
        .service(hello_path2)
        .service(hello_json)
        .service(hello_enum)
    )
        .bind("0.0.0.0:4000")?
        .run()
        .await
}

#[cfg(test)]
mod tests {
    use actix_web::client::Client;
    use crate::HelloQuery;

    #[actix_rt::test]
    async fn test_version() {
        let mut resp = Client::default().get("http://localhost:4000/version")
            .send()
            .await
            .unwrap();
        println!("Response: {:?}", resp);
        println!("Body: {:?}", resp.body().await.unwrap());
    }

    #[actix_rt::test]
    async fn test_post() {
        let mut resp = Client::default().post("http://localhost:4000/hello/json")
            .send_json(&HelloQuery{ name: String::from("123") })
            .await
            .unwrap();
        println!("Response: {:?}", resp);
        println!("Body: {:?}", resp.body().await.unwrap());
    }
}
